import {User} from '../user.model';
import {AuthActions, CLEAR_ERROR, LOGIN, LOGIN_FAIL, LOGIN_SUCCESS, LOGOUT, SIGNUP, SIGNUP_FAIL, SIGNUP_SUCCESS} from './auth.actions';

export interface State {
  user: User;
  authError: string;
  loading: boolean;
}

const initialState: State = {
  user: null,
  authError: null,
  loading: false
};

export function authReducer(state = initialState, action: AuthActions): State {
  switch (action.type) {
    case SIGNUP:
    case LOGIN:
      return {
        ...state,
        authError: null,
        loading: true
      };
    case SIGNUP_SUCCESS:
    case LOGIN_SUCCESS:
      const {email, userId, token, expirationDate} = action.payload;
      const user = new User(email, userId, token, expirationDate);
      return {
        ...state,
        authError: null,
        user,
        loading: false
      };
    case SIGNUP_FAIL:
    case LOGIN_FAIL:
      return {
        ...state,
        authError: action.payload,
        loading: false
      };
    case LOGOUT:
      return {
        ...state,
        user: null
      };
    case CLEAR_ERROR:
      return {
        ...state,
        authError: null
      };
    default:
      return state;
  }
}
